package edu.towson.cis.cosc442.project4.coffeemaker;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.After;

public class CoffeeMakerTest {

    private CoffeeMaker coffeeMaker;
    private Inventory inventory;
    Recipe coffeeRecipe;
    Recipe latteRecipe;

    @Before
    public void setUp() {
        coffeeMaker = new CoffeeMaker();
        inventory = coffeeMaker.checkInventory();
        
        coffeeRecipe = new Recipe();
        coffeeRecipe.setName("Coffee");
        coffeeRecipe.setPrice(10);
        coffeeRecipe.setAmtCoffee(2);
        coffeeRecipe.setAmtMilk(2);
        coffeeRecipe.setAmtSugar(2);
        coffeeRecipe.setAmtChocolate(2);
        coffeeMaker.addRecipe(coffeeRecipe);
        
        latteRecipe = new Recipe();
        latteRecipe.setName("Latte");
        latteRecipe.setPrice(5);
        latteRecipe.setAmtCoffee(1);
        latteRecipe.setAmtMilk(1);
        latteRecipe.setAmtSugar(1);
        latteRecipe.setAmtChocolate(1);
        coffeeMaker.addRecipe(latteRecipe);
        
    }
    
    @After
    public void tearDown() {        
        Recipe[] recipes = coffeeMaker.getRecipes();
        for (int i = 0; i < recipes.length; i++) {
            recipes[i] = null;
        }
        
        coffeeMaker = null;
        inventory = null;
    }
    

    @Test
    public void testAddDupeRecipe() {
        Recipe testRecipe = new Recipe();
        testRecipe.setName("Test");
        assertTrue(coffeeMaker.addRecipe(testRecipe));
        assertFalse(coffeeMaker.addRecipe(testRecipe));
    }
    
    @Test
    public void testAddRecipeFull() {
        Recipe test1 = new Recipe();
        test1.setName("test1");
        Recipe test2 = new Recipe();
        test2.setName("test2");
        assertTrue(coffeeMaker.addRecipe(test1));
        assertTrue(coffeeMaker.addRecipe(test2));
        assertFalse(coffeeMaker.addRecipe(new Recipe()));
    }

    @Test
    public void testDeleteRecipe() {
        Recipe test1 = new Recipe();
        test1.setName("test1");
        coffeeMaker.addRecipe(test1);
        assertTrue(coffeeMaker.deleteRecipe(test1));
        assertFalse(coffeeMaker.deleteRecipe(test1));
    }
    
    @Test
    public void testSetCoffee() {
        // Test setting a valid amount of coffee
        Inventory.setCoffee(10);
        assertEquals(10, inventory.getCoffee());

        // Test setting a negative amount of coffee
        Inventory.setCoffee(-5);
        assertEquals(0, inventory.getCoffee());
        
        // Test setting a zero amount of coffee
        Inventory.setCoffee(0);
        assertEquals(0, inventory.getCoffee());
        
        Inventory.setCoffee(1);
        assertEquals(1, inventory.getCoffee());
    }
    
    @Test
    public void testSetMilk() {
        // Test setting a valid amount of milk
        Inventory.setMilk(20);
        assertEquals(20, inventory.getMilk());

        // Test setting a negative amount of milk
        Inventory.setMilk(-10);
        assertEquals(0, inventory.getMilk());
        
        Inventory.setMilk(0);
        assertEquals(0, inventory.getMilk());
        
        Inventory.setMilk(1);
        assertEquals(1, inventory.getMilk());
    }
    
    @Test
    public void testSetSugar() {
        // Test setting a valid amount of sugar
        Inventory.setSugar(12);
        assertEquals(12, inventory.getSugar());

        // Test setting a negative amount of sugar
        Inventory.setSugar(-8);
        assertEquals(0, inventory.getSugar());
        
        Inventory.setSugar(0);
        assertEquals(0, inventory.getSugar());
        
        Inventory.setSugar(1);
        assertEquals(1, inventory.getSugar());
    }
    
    @Test
    public void testSetChocolate() {
        // Test setting a valid amount of chocolate
        Inventory.setChocolate(25);
        assertEquals(25, inventory.getChocolate());

        // Test setting a negative amount of chocolate
        Inventory.setChocolate(-12);
        assertEquals(0, inventory.getChocolate());
        
        Inventory.setChocolate(0);
        assertEquals(0, inventory.getChocolate());
        
        Inventory.setChocolate(1);
        assertEquals(1, inventory.getChocolate());
    }

    @Test
    public void testEditRecipe() {
        Recipe oldRecipe = new Recipe();
        oldRecipe.setName("Old");
        coffeeMaker.addRecipe(oldRecipe);

        Recipe newRecipe = new Recipe();
        newRecipe.setName("New");
        newRecipe.setPrice(10);
        assertTrue(coffeeMaker.editRecipe(oldRecipe, newRecipe));
    }

    @Test
    public void testAddInventory() {
        assertTrue(coffeeMaker.addInventory(10, 10, 10, 10));
        assertFalse(coffeeMaker.addInventory(-10, -10, -10, -10));
    }
    
    @Test
    public void testAddInventoryWithValidAmounts() {
        coffeeMaker.addInventory(10, 10, 10, 10);
        inventory = coffeeMaker.checkInventory();
        assertEquals(10, inventory.getCoffee());
        assertEquals(10, inventory.getMilk());
        assertEquals(10, inventory.getSugar());
        assertEquals(10, inventory.getChocolate());
    }
    
    @Test
    public void testAddInventoryMultipleTimes() {
        coffeeMaker.addInventory(5, 5, 5, 5);
        coffeeMaker.addInventory(10, 10, 10, 10);
        inventory = coffeeMaker.checkInventory();
        assertEquals(15, inventory.getCoffee());
        assertEquals(15, inventory.getMilk());
        assertEquals(15, inventory.getSugar());
        assertEquals(15, inventory.getChocolate());
    }
    
    @Test
    public void testAddInventoryWithZeroAmounts() {
        // Add zero amounts of ingredients
        assertTrue(coffeeMaker.addInventory(0, 0, 0, 0));

        inventory = coffeeMaker.checkInventory();
        assertEquals(0, inventory.getCoffee());
        assertEquals(0, inventory.getMilk());
        assertEquals(0, inventory.getSugar());
        assertEquals(0, inventory.getChocolate());
    }
    
    @Test
    public void testAddInventoryWithNegativeAmounts() {
        // Add negative amounts of ingredients
        assertFalse(coffeeMaker.addInventory(-5, -5, -5, -5));

        inventory = coffeeMaker.checkInventory();
        assertEquals(0, inventory.getCoffee());
        assertEquals(0, inventory.getMilk());
        assertEquals(0, inventory.getSugar());
        assertEquals(0, inventory.getChocolate());
    }
    
    @Test
    public void testMakeCoffeeWithInsufficientInventory() {
        // Create a recipe that requires more ingredients than available in the inventory
        Recipe big = new Recipe();
        big.setName("Insufficient");
        big.setPrice(100);
        big.setAmtCoffee(500);
        big.setAmtMilk(500);
        big.setAmtSugar(500);
        big.setAmtChocolate(500);

        // Add the recipe to the coffee maker
        coffeeMaker.addRecipe(big);

        // Make coffee with the recipe and insufficient payment
        int payment = 5;
        int change = coffeeMaker.makeCoffee(big, payment);

        // Assert that the method returns the correct change
        assertEquals(payment, change);

        // Assert that the inventory remains unchanged
        inventory = coffeeMaker.checkInventory();
        assertEquals(0, inventory.getCoffee());
        assertEquals(0, inventory.getMilk());
        assertEquals(0, inventory.getSugar());
        assertEquals(0, inventory.getChocolate());
    }
    
    @Test
    public void testCheckInventory() {
        inventory = coffeeMaker.checkInventory();
        assertNotNull(inventory);
    }

    @Test
    public void testMakeCoffee() {
        coffeeMaker.addInventory(10, 10, 10, 10);
        assertEquals(5, coffeeMaker.makeCoffee(coffeeRecipe, 15));
        assertEquals(0, coffeeMaker.makeCoffee(latteRecipe, 5));
    }

    @Test
    public void testGetRecipes() {
        Recipe[] recipes = coffeeMaker.getRecipes();
        assertNotNull(recipes);
        assertEquals(4, recipes.length);
    }

    @Test
    public void testGetRecipeForName() {

        assertEquals(coffeeRecipe, coffeeMaker.getRecipeForName("Coffee"));
        assertNull(coffeeMaker.getRecipeForName("Tea"));
    }
}