package edu.towson.cis.cosc442.project4.coffeemaker;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import org.junit.After;

public class InventoryTest {

    private Inventory inventory;

    @Before
    public void setUp() {
        inventory = new Inventory();
    }
    
    @After
    public void tearDown() {        
    	inventory = null;
        }

    @Test
    public void testGettersAndSetters() {
        Inventory.setCoffee(15);
        Inventory.setMilk(15);
        Inventory.setSugar(15);
        Inventory.setChocolate(15);
        
        assertEquals(15, inventory.getCoffee());
        assertEquals(15, inventory.getMilk());
        assertEquals(15, inventory.getSugar());
        assertEquals(15, inventory.getChocolate());
    }

    @Test
    public void testSettersWithNegativeValues() {
        Inventory.setCoffee(-10);
        Inventory.setMilk(-20);
        Inventory.setSugar(-30);
        Inventory.setChocolate(-40);

        assertEquals(0, inventory.getCoffee());
        assertEquals(0, inventory.getMilk());
        assertEquals(0, inventory.getSugar());
        assertEquals(0, inventory.getChocolate());
    }

    @Test
    public void testEnoughIngredients() {
        Inventory.setCoffee(15);
        Inventory.setMilk(15);
        Inventory.setSugar(15);
        Inventory.setChocolate(15);

        Recipe recipe = new Recipe();
        recipe.setAmtCoffee(10);
        recipe.setAmtMilk(10);
        recipe.setAmtSugar(10);
        recipe.setAmtChocolate(10);

        assertTrue(inventory.enoughIngredients(recipe));
    }

    @Test
    public void testToString() {
        Inventory.setCoffee(10);
        Inventory.setMilk(5);
        Inventory.setSugar(8);
        Inventory.setChocolate(3);

        String expected = "Coffee: 10\nMilk: 5\nSugar: 8\nChocolate: 3\n";
        assertEquals(expected, inventory.toString());
    }
    
    @Test
    public void testSetChocolateAtBoundary() {
        Inventory.setChocolate(0);
        assertEquals(0, inventory.getChocolate());
        Inventory.setChocolate(1);
        assertEquals(1, inventory.getChocolate());
    }

    @Test
    public void testNotEnoughIngredients() {
        Inventory.setCoffee(1);
        Inventory.setMilk(1);
        Inventory.setSugar(1);
        Inventory.setChocolate(1);
        Recipe recipe = new Recipe();
        recipe.setAmtCoffee(2);
        recipe.setAmtMilk(2);
        recipe.setAmtSugar(2);
        recipe.setAmtChocolate(2);
        assertFalse(new Inventory().enoughIngredients(recipe));
    }
    
    @Test
    public void testEnoughIngredientsWithEqualAmounts() {
        Inventory.setCoffee(10);
        Inventory.setMilk(10);
        Inventory.setSugar(10);
        Inventory.setChocolate(10);

        Recipe recipe = new Recipe();
        recipe.setAmtCoffee(10);
        recipe.setAmtMilk(10);
        recipe.setAmtSugar(10);
        recipe.setAmtChocolate(10);

        assertTrue(inventory.enoughIngredients(recipe));
    }
}