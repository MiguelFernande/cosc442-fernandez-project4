package edu.towson.cis.cosc442.project4.coffeemaker;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class RecipeTest {

    private Recipe recipe;

    @Before
    public void setUp() {
        recipe = new Recipe();
    }

    @Test
    public void testGettersAndSetters() {
        recipe.setName("Coffee");
        recipe.setPrice(10);
        recipe.setAmtCoffee(3);
        recipe.setAmtMilk(2);
        recipe.setAmtSugar(1);
        recipe.setAmtChocolate(0);

        assertEquals("Coffee", recipe.getName());
        assertEquals(10, recipe.getPrice());
        assertEquals(3, recipe.getAmtCoffee());
        assertEquals(2, recipe.getAmtMilk());
        assertEquals(1, recipe.getAmtSugar());
        assertEquals(0, recipe.getAmtChocolate());
    }

    @Test
    public void testSettersWithNegativeValues() {
        recipe.setPrice(-10);
        recipe.setAmtCoffee(-3);
        recipe.setAmtMilk(-2);
        recipe.setAmtSugar(-1);
        recipe.setAmtChocolate(-1);

        assertEquals(0, recipe.getPrice());
        assertEquals(0, recipe.getAmtCoffee());
        assertEquals(0, recipe.getAmtMilk());
        assertEquals(0, recipe.getAmtSugar());
        assertEquals(0, recipe.getAmtChocolate());
    }

    @Test
    public void testEquals() {
        Recipe recipe1 = new Recipe();
        recipe1.setName("Coffee");

        Recipe recipe2 = new Recipe();
        recipe2.setName("Coffee");

        Recipe recipe3 = new Recipe();
        recipe3.setName("Tea");

        assertTrue(recipe1.equals(recipe2));
        assertFalse(recipe1.equals(recipe3));
    }

    @Test
    public void testToString() {
        recipe.setName("Coffee");
        assertEquals("Coffee", recipe.toString());
    }
    
    @Test
    public void testGetAmtChocolate() {
        Recipe recipe = new Recipe();
        recipe.setAmtChocolate(5);
        assertEquals(5, recipe.getAmtChocolate());
    }
    
    @Test
    public void testSetAmtChocolateAtBoundary() {
        Recipe recipe = new Recipe();
        recipe.setAmtChocolate(0);
        assertEquals(0, recipe.getAmtChocolate());
        recipe.setAmtChocolate(1);
        assertEquals(1, recipe.getAmtChocolate());
        recipe.setAmtChocolate(-1);
        assertEquals(0, recipe.getAmtChocolate());
    }

    @Test
    public void testSetAmtMilkAtBoundary() {
        Recipe recipe = new Recipe();
        recipe.setAmtMilk(0);
        assertEquals(0, recipe.getAmtMilk());
        recipe.setAmtMilk(1);
        assertEquals(1, recipe.getAmtMilk());
        recipe.setAmtMilk(-1);
        assertEquals(0, recipe.getAmtMilk());
    }

    @Test
    public void testSetAmtSugarAtBoundary() {
        Recipe recipe = new Recipe();
        recipe.setAmtSugar(0);
        assertEquals(0, recipe.getAmtSugar());
        recipe.setAmtSugar(1);
        assertEquals(1, recipe.getAmtSugar());
        recipe.setAmtSugar(-1);
        assertEquals(0, recipe.getAmtSugar());
    }

    @Test
    public void testSetPriceAtBoundary() {
        Recipe recipe = new Recipe();
        recipe.setPrice(0);
        assertEquals(0, recipe.getPrice());
        recipe.setPrice(1);
        assertEquals(1, recipe.getPrice());
        recipe.setPrice(-1);
        assertEquals(0, recipe.getPrice());
    }

    @Test
    public void testEqualsWithNullRecipe() {
        Recipe recipe = new Recipe();
        recipe.setName("Recipe");
        assertFalse(recipe.equals(null));
    }

    @Test
    public void testEqualsWithNullName() {
        Recipe recipe1 = new Recipe();
        Recipe recipe2 = new Recipe();
        recipe2.setName("Recipe");
        assertFalse(recipe1.equals(recipe2));
        assertFalse(recipe2.equals(recipe1));
    }
    
}